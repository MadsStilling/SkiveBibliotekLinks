Som en udvikler på denne app skal man kunne bruge [Git](https://git-scm.com/).

Dette holder styr på projektet på en måde så alle, der arbejder på projektet kan flette dokumenter branches osv.
Dette er næsten umuligt uden Git e.lign system. Tænk at skulle kigge hvert dokument igennem, linje for linje, for at finde den ene linje den ene stavefejl din kollega rettede. Den ene rettesle kunne også være en stor, ny funktion der ville være slettet, hvis den ikke var fundet.

### Hvordan starter jeg?

1. Det *første* du altid gør som udvikler er, at gå ind på projektsiden for at se, hvilke Issues der er, hvad der skal ændres på hvilken branch osv.

2. Derefter går du ind i Git og navigerer til projektet.

3. Sikrer dig, at du er på den rigtige branch.

4. 
```bash
$ git pull

Nu er du klar til at arbejde. Det virker måske som mange trin, men når man arbejder med andre garanterer jeg, at det er besværet værd.

### Git branch
Du kan lave lige så mange branches/grene du vil. Du kan endda lave branches på branches.  For at lave en ny branch skal du bare skrive `$ git branch din-nye-branches-navn` og sså har du oprettet den. Du skifter til din nye branch med `$ git checkout din-nye-branches-navn`.

Alternativt kan du slå to fluer med ét smæk ved at skrive:
```bash
$ git checkout -b din-nye-branches-navn
```
-b betyder at du opretter en ny branch.

Slet en branch ved først, at sikre dig at du ikke befinder dig på branchen med `$ git branch`.  Du kan ikke save den gren af du sidder på :ambulance:.
```bash
$ git branch -d din-nye-branches-navn
```
Dette sletter din branch lokalt (på computeren).  For at slette branchen på din remote/serveren/github/gitlab, skal du bruge kommandoen:
```bash
$ git push origin :din-nye-branches-navn
```
NB! Husk at skrive : foran branch navnet.





### Jeg har lavet bøvl i koden, hvad nu?
Hvis du har siddet og leget lidt med koden og ingenting virker.
Ingen panik! det sker konstant for udvilkere. Med git i projektmappen skal du skrive følgene:
```bash
$ git reset --hard HEAD
```
OBS! Kun brug --hard, hvis du er sikker på at du ikke vil have din ændrede kode tilbage.

Dette tager koden tilbage til sidste `commit`.
```
      (D)
A-B-C
       ↑
```
Lad os sige at A-B-C er dine commits og C er den nyeste. (D) er dine ændringer, som du fortryder.
Overstående kommando gør følgende:
```
   (C)
A-B-C
    ↑
```
### Jeg har fortrudt et `commit`, hvad gør jeg?
```bash
$ git reset HEAD~1
```
dette tager dig tilbage til dit `commit` før fejlen.
Før:
```
   (D)
A-B-C
    ↑
```
Efter:
```
   (D)
A-B-C
  ↑
```
Som du kan se er ligger det gamle commit der stadig, men Git peger på et ældre.
Du har stadig dine ændringer, men kan nu overskrive det nyeste commit.

Brug --hard, hvis du ikke vil gemme dine ændringer i dit "fejl-commit".
```
 (B)
A-B
  ↑
```

### Branches. Hvordan gør jeg og hvorfor virker `$ git push` ikke?
Når du laver en ny branch er det som en kopi af master branchen.
```bash
$ git checkout -b <branch_navn>
```
Dette laver en ny branch og går ind på den. det er en forkortelse af:
```bash
$ git branch <branch_navn>
$ git checkout <branch_navn>
```
når du har lavet `$ git commit` på dine ændringer kan du pushe til remote server. Det er "hjemmesiden" du uploader dit arbejde til. Måden du pusher dine ændringer til remoten er ved:
```bash
$ git push -u origin <branch_navn>
```
Du kan finde mere information om branhes, merges osv [her](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)