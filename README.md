# SkiveBibliotekLinks

This app is used by Skive Bibliotek (https://www.skivebibliotek.dk/) on a huge
 touch screen to promote URLs.

The idea is to promote links/URLs chosen by the library staff without disrupting their webpage design.

The app is made to be easy to edit, so it can be reused. Documentation is included, but only in danish.
