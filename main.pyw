from time import sleep
import subprocess
import platform
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.properties import StringProperty
from kivy.clock import Clock

#TODO Make a function to split 'links' file or import module

def open_file(file_name):
    with open(file_name) as f:
        f = f.readlines()
    return f


class CButton(Button):
    #Custom button
    background_normal = StringProperty('Images/button.png')
    background_down = StringProperty('Images/button_pressed.png')


class WidgetContainer(BoxLayout):
    pass


class Buttons(BoxLayout):
    #Puts all our ducks.. I mean buttons in a row
    def __init__(self, **kwargs):
        super(Buttons, self).__init__()
        self.orientation = 'vertical'
        build = self.build_buttons('links.csv')
        link = self.link
        self.seconds = 0
        Clock.schedule_interval(self.update_clock, 1)
        self.program = 0
        self.browser_timelimit = 900 #900 = 15 minutes
        self.system = platform.system()

    def check_os(self, link):
        try:
            self.program = subprocess.Popen(str("C:\Program Files\Internet Explorer\IEXPLORE.EXE " + link))
        except:
            self.program = subprocess.Popen(["firefox", link])


    def build_buttons(self, filename):
        #Builds buttons from the file containing text and links to the buttons
        for btn in open_file(filename):
            btn = btn.split(',')
            button = CButton(text=str(btn[0]),size=self.size, on_release=self.callback)
            self.add_widget(button)
            self.link = str(btn[1])

    def update_clock(self, *arg):
        # Stops browser if timelimit is reached
        if self.seconds >= self.browser_timelimit:
            self.seconds = 0
            self.program.kill()
            #Stops self.seconds from increasing
            self.program = 0
        elif self.seconds <= self.browser_timelimit and type(self.program) != int:
            self.seconds = self.seconds + 1

    def callback(self, instance):
        #Opens link in browser matching the pressed buttons name
        for column in open_file('links.csv'):
            column = column.split(',')
            if instance.text in column:
                self.seconds = 0
                self.check_os(column[1])



class QR(Buttons):
    #Fetches all QR codes to put next to buttons
    def __init__(self, **kwargs):
        super(Buttons, self).__init__()
        self.orientation = 'vertical'
        build = self.build_qr_codes('links.csv')

    def build_qr_codes(self, filename):
        for line in open_file(filename):
            line = line.split(',')
            image = str('Images/' + line[0].lower() + '.png')
            image = Image(source=image)
            qr = self.add_widget(image)


class ButtonLinks(App):
    def build(self):
        return WidgetContainer()

if __name__ == '__main__':
    ButtonLinks().run()
